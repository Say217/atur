package id.ac.ui.cs.mobileprogramming.sayid_abyan_rizal_shiddiq.atur;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import id.ac.ui.cs.mobileprogramming.sayid_abyan_rizal_shiddiq.atur.dao.ExpenditureDAO;
import id.ac.ui.cs.mobileprogramming.sayid_abyan_rizal_shiddiq.atur.entity.Expenditure;

@Database(entities = {Expenditure.class}, version = 1, exportSchema = false)
public abstract class AturDatabase extends RoomDatabase {
    private static final String DB_NAME = "atur_db";
    public static volatile AturDatabase INSTANCE;

    public abstract ExpenditureDAO getExpenditureDAO();

    public static AturDatabase getInstance(Context context) {
        if (INSTANCE == null) {
            synchronized (AturDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AturDatabase.class, DB_NAME)
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}
