package id.ac.ui.cs.mobileprogramming.sayid_abyan_rizal_shiddiq.atur;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class AturBroadcastReceiver extends BroadcastReceiver {
    private Toast toast;

    @Override
    public void onReceive(Context context, Intent intent) {
        if(Intent.ACTION_DATE_CHANGED.equals(intent.getAction())){
            toast = Toast.makeText(context, context.getString(R.string.toast_broadcast), Toast.LENGTH_SHORT);
            toast.show();
        }
    }
}
