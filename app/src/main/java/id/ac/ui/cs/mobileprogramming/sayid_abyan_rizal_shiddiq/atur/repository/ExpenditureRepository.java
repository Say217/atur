package id.ac.ui.cs.mobileprogramming.sayid_abyan_rizal_shiddiq.atur.repository;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;

import java.util.List;

import id.ac.ui.cs.mobileprogramming.sayid_abyan_rizal_shiddiq.atur.AturDatabase;
import id.ac.ui.cs.mobileprogramming.sayid_abyan_rizal_shiddiq.atur.dao.ExpenditureDAO;
import id.ac.ui.cs.mobileprogramming.sayid_abyan_rizal_shiddiq.atur.entity.Expenditure;

public class ExpenditureRepository {
    private ExpenditureDAO expenditureDao;

    public ExpenditureRepository(Application application) {
        AturDatabase db = AturDatabase.getInstance(application);
        expenditureDao = db.getExpenditureDAO();
    }

    public LiveData<List<Expenditure>> getAllExpenditure() {
        Log.d("DEBUGGER", "" + expenditureDao.getAllExpenditure().getValue());
        return expenditureDao.getAllExpenditure();
    }

    public void insertExpenditureAsyncTask(Expenditure expenditure){
        new InsertExpenditureAsyncTask(expenditureDao).execute(expenditure);
    }

    public void updateExpenditureAsyncTask(Expenditure expenditure){
        new UpdateExpenditureAsyncTask(expenditureDao).execute(expenditure);
    }

    public void deleteAll() {
        new DeleteAllExpendituresAsyncTask(expenditureDao).execute();
    }

    private static class DeleteAllExpendituresAsyncTask extends AsyncTask<Expenditure, Void, Void> {
        private ExpenditureDAO expenditureDao;

        public DeleteAllExpendituresAsyncTask(ExpenditureDAO expenditureDao) {
            this.expenditureDao = expenditureDao;
        }

        @Override
        protected Void doInBackground(Expenditure... expenditures) {
            expenditureDao.deleteAll();
            return null;
        }
    }

    private static class UpdateExpenditureAsyncTask extends AsyncTask<Expenditure, Void, Void>{
        private ExpenditureDAO expenditureDao;

        private UpdateExpenditureAsyncTask(ExpenditureDAO expenditureDao){
            this.expenditureDao = expenditureDao;
        }

        @Override
        protected Void doInBackground(Expenditure... expenditures) {
            expenditureDao.update(expenditures[0]);
            return null;
        }
    }


   private static class InsertExpenditureAsyncTask extends AsyncTask<Expenditure, Void, Void>{
        private ExpenditureDAO expenditureDao;

        private InsertExpenditureAsyncTask(ExpenditureDAO expenditureDao){
            this.expenditureDao = expenditureDao;
        }

        @Override
        protected Void doInBackground(Expenditure... expenditures) {
            expenditureDao.insert(expenditures[0]);
            return null;
        }
    }
}
