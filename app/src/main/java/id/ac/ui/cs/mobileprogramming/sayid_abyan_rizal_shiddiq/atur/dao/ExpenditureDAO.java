package id.ac.ui.cs.mobileprogramming.sayid_abyan_rizal_shiddiq.atur.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import id.ac.ui.cs.mobileprogramming.sayid_abyan_rizal_shiddiq.atur.entity.Expenditure;

@Dao
public interface ExpenditureDAO {

    @Insert
    void insert(Expenditure expenditure);

    @Update
    void update(Expenditure expenditure);

    @Query("SELECT * from expenditure")
    LiveData<List<Expenditure>> getAllExpenditure();

    @Query("DELETE from expenditure")
    void deleteAll();
}
