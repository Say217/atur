package id.ac.ui.cs.mobileprogramming.sayid_abyan_rizal_shiddiq.atur.ui.main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import id.ac.ui.cs.mobileprogramming.sayid_abyan_rizal_shiddiq.atur.R;
import id.ac.ui.cs.mobileprogramming.sayid_abyan_rizal_shiddiq.atur.adapter.ExpenditureAdapter;
import id.ac.ui.cs.mobileprogramming.sayid_abyan_rizal_shiddiq.atur.entity.Expenditure;

public class MainFragment extends Fragment implements ExpenditureAdapter.CardListener {

    private ExpenditureViewModel expenditureViewModel;
    private FloatingActionButton addCategoryButton;
    private FloatingActionButton deleteAllButton;
    private ExpenditureAdapter adapter;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.main_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        expenditureViewModel = ViewModelProviders.of(this).get(ExpenditureViewModel.class);
        RecyclerView recyclerView = getView().findViewById(R.id.recycler_view);
        adapter = new ExpenditureAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);
        expenditureViewModel.getAllExpenditures().observe(this, new Observer<List<Expenditure>>() {
            @Override
            public void onChanged(List<Expenditure> expenditures) {
                adapter.setExpenditures(expenditures);
            }
        });

        addCategoryButton = getView().findViewById(R.id.add_item);
        deleteAllButton = getView().findViewById(R.id.delete_all);

        addCategoryButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                addCategoryButton.hide();
                deleteAllButton.hide();
                getFragmentManager().beginTransaction()
                        .replace(R.id.main, AddExpFragment.newInstance(), "ADD_FRAGMENT")
                        .addToBackStack(null)
                        .commit();
            }
        });

        deleteAllButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                expenditureViewModel.deleteAll();
            }
        });

    }

    @Override
    public void onCardListener(int position, boolean addClicked, View view) {
        Expenditure expenditure = adapter.getExpenditureAt(position);
        if(addClicked){
            EditText spendForm =view.findViewById(R.id.spend_form);
            String added = spendForm.getText().toString();
            spendForm.getText().clear();
             if(!added.equals("")){
                int addedInt = Integer.parseInt(added);
                expenditure.setCurrent(expenditure.getCurrent() + addedInt);
                expenditureViewModel.update(expenditure);
            }
            else{
                Toast.makeText(getContext(), getString(R.string.spend_form_not_filled), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
