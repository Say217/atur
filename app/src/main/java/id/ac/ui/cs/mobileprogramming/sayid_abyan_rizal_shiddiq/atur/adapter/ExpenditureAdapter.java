package id.ac.ui.cs.mobileprogramming.sayid_abyan_rizal_shiddiq.atur.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import id.ac.ui.cs.mobileprogramming.sayid_abyan_rizal_shiddiq.atur.R;
import id.ac.ui.cs.mobileprogramming.sayid_abyan_rizal_shiddiq.atur.entity.Expenditure;
import id.ac.ui.cs.mobileprogramming.sayid_abyan_rizal_shiddiq.atur.ui.main.MainFragment;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class ExpenditureAdapter extends RecyclerView.Adapter<ExpenditureAdapter.ExpenditureViewHolder> {
    private List<Expenditure> expenditures = new ArrayList<>();
    private CardListener cardListener ;

    public ExpenditureAdapter(MainFragment cardListener){
        this.cardListener =  cardListener;
    }

    @NonNull
    @Override
    public ExpenditureViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.expenditure_item, parent, false);
        return new ExpenditureViewHolder(itemView, cardListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ExpenditureViewHolder holder, int position) {
        final Expenditure currentExpenditure = expenditures.get(position);
        holder.expName.setText(currentExpenditure.getName());
        holder.expLimit.setText(String.valueOf(currentExpenditure.getLimit()));
        holder.expCurrent.setText(String.valueOf(currentExpenditure.getCurrent()));
    }

    @Override
    public int getItemCount() {
        return expenditures.size();
    }

    public void setExpenditures(List<Expenditure> expenditures){
        this.expenditures = expenditures;
        notifyDataSetChanged();
    }

    public Expenditure getExpenditureAt(int position){
        return expenditures.get(position);
    }

    class ExpenditureViewHolder extends  RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView expName;
        private TextView expLimit;
        private  TextView expCurrent;
        private Button expAdd;
        private CardListener cardListener;

        public ExpenditureViewHolder(View itemView, CardListener cardListener){
            super(itemView);
            this.expName = itemView.findViewById(R.id.exp_name);
            this.expLimit = itemView.findViewById(R.id.exp_limit);
            this.expCurrent = itemView.findViewById(R.id.exp_current);
            this.expAdd = itemView.findViewById(R.id.exp_add);
            this.cardListener = cardListener;;

            expAdd.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == expAdd.getId()){
                Log.e(TAG, "onClick: ADD CLICKED");
                cardListener.onCardListener(getAdapterPosition(), true, itemView);
            }
            else {
                Log.e(TAG, "onClick: CARD CLICKED");
                cardListener.onCardListener(getAdapterPosition(), false, itemView);
            }
        }
    }

    public interface CardListener{
        void onCardListener(int position, boolean addClicked, View view);
    }
}
