package id.ac.ui.cs.mobileprogramming.sayid_abyan_rizal_shiddiq.atur.entity;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "expenditure")
public class Expenditure {

    @PrimaryKey
    @NonNull
    private String name;
    private int limit;
    private int current;

    public Expenditure(@NonNull String name, int limit) {
        this.name = name;
        this.limit = limit;
        this.current = 0;
    }

    @NonNull
    public String getName() {
        return this.name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    public int getLimit(){
        return this.limit;
    }

    public void setLimit(int limit) {
    this.limit = limit;
    }

    public int getCurrent(){
        return this.current;
    }

    public void setCurrent(int current) {
        this.current = current;
    }
}