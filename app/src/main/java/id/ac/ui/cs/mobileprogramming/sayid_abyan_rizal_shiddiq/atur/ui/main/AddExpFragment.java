package id.ac.ui.cs.mobileprogramming.sayid_abyan_rizal_shiddiq.atur.ui.main;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import id.ac.ui.cs.mobileprogramming.sayid_abyan_rizal_shiddiq.atur.R;
import id.ac.ui.cs.mobileprogramming.sayid_abyan_rizal_shiddiq.atur.entity.Expenditure;

public class AddExpFragment extends Fragment {
    private EditText enterCategory;
    private EditText enterLimit;
    private ExpenditureViewModel expenditureViewModel;

    public static AddExpFragment newInstance() {
        return new AddExpFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_exp, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        enterCategory = getView().findViewById(R.id.enter_name);
        enterLimit = getView().findViewById(R.id.enter_limit);
        expenditureViewModel = ViewModelProviders.of(this).get(ExpenditureViewModel.class);
    }

    private void saveCategory() {
        String category = enterCategory.getText().toString();
        String limit = enterLimit.getText().toString();

        if(category.trim().isEmpty() || limit.trim().isEmpty()){
            Toast.makeText(this.getContext(),getString(R.string.form_not_filled) , Toast.LENGTH_SHORT).show();
            return;
        }
        Expenditure expenditure = new Expenditure(category, Integer.parseInt(limit));
        expenditureViewModel.insert(expenditure);

        Toast.makeText(getContext(),getContext().getString(R.string.form_filled) , Toast.LENGTH_SHORT).show();
        getActivity().onBackPressed();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.add_category, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.save_category:
                saveCategory();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
