package id.ac.ui.cs.mobileprogramming.sayid_abyan_rizal_shiddiq.atur;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import id.ac.ui.cs.mobileprogramming.sayid_abyan_rizal_shiddiq.atur.ui.main.ExpenditureViewModel;
import id.ac.ui.cs.mobileprogramming.sayid_abyan_rizal_shiddiq.atur.ui.main.MainFragment;

public class MainActivity extends AppCompatActivity {
    AturBroadcastReceiver aturBroadcastReceiver = new AturBroadcastReceiver();
    private ExpenditureViewModel expenditureViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.e("MainActivity", "entered");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        super.onStart();

        IntentFilter filter = new IntentFilter(Intent.ACTION_DATE_CHANGED) ;
        registerReceiver(aturBroadcastReceiver,filter);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, MainFragment.newInstance())
                    .commitNow();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(aturBroadcastReceiver);
    }

    @Override
    public void onBackPressed() {
        FloatingActionButton fab = findViewById(R.id.add_item);
        FloatingActionButton fab1 = findViewById(R.id.delete_all);
        fab.show();
        fab1.show();
        super.onBackPressed();
    }
}
