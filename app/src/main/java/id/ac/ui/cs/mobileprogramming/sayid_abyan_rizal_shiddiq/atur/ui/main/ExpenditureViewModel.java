package id.ac.ui.cs.mobileprogramming.sayid_abyan_rizal_shiddiq.atur.ui.main;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import id.ac.ui.cs.mobileprogramming.sayid_abyan_rizal_shiddiq.atur.repository.ExpenditureRepository;
import id.ac.ui.cs.mobileprogramming.sayid_abyan_rizal_shiddiq.atur.entity.Expenditure;

public class ExpenditureViewModel extends AndroidViewModel {
    private ExpenditureRepository repository;
    public List<Expenditure> expenditures;
    private LiveData<List<Expenditure>> allExpenditures;

    public ExpenditureViewModel(@NonNull Application application) {
        super(application);
        repository = new ExpenditureRepository(application);
        allExpenditures = repository.getAllExpenditure();
    }

    public void insert(Expenditure expenditure){
        repository.insertExpenditureAsyncTask(expenditure);
    }

    public void update(Expenditure expenditure){
        repository.updateExpenditureAsyncTask((expenditure));
    }

    public void deleteAll(){
        repository.deleteAll();
    }

    public LiveData<List<Expenditure>> getAllExpenditures() {
        return allExpenditures;
    }
}
